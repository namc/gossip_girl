Gossip Girl
------

### Components 

* Node.js dev-environment
* Node.js modules : Express, Underscore, Nodemailer
* MongoDb 
* Testing modules : Mocha, Should, Supertest (optional)

### Backend 

* MongoDb is used as storage for service.
* Use of collections instead of object mapper

### APIs

The APIs are built on Express framework since it's robust for web-apps

* For incoming requests

```javascript
app.get('/events', event.findAll);
app.get('/events/:id', event.findById);
app.post('/events', event.addEvent);
app.put('/events/:id', event.updateEvent);
app.delete('/events/:id', event.deleteEvent);
```

* For subscriptions 

```javascript
app.get('/subscriptions', sub.findAll);
app.get('/subscriptions/:id', sub.findById);
app.post('/subscriptions', sub.addSubscription);
app.put('/subscriptions/:id', sub.updateSubscription);
app.delete('/subscriptions/:id', sub.deleteSubscription);
```

* For signals

```javascript
app.post('/signals', signal.processSignal);
app.get('/signallog', signallog.findRecent); 
```

### Notification

* A signal is received through the signals route. 

```javascript
app.post('/signals', signal.processSignal);
```

* A connection to the database is established. We filter collection to get all subscriptions with a matching Event.

```javascript
function processMatch(subscription, signal) {
	// ..code
}
```

* Email notification sent through NodeMailer

```javascript
exports.sendMail = function (opts) {
	// ..code
}
```









