Gossip Girl
------

### References 

1. [Nodemailer Documentation](http://nodemailer.com/)
2. mailer.js was almost entirely adapted from [this blog](http://blog.ragingflame.co.za/2013/2/14/roll-out-your-own-uptime-monitor-with-nodejs)
3. [Markdown Cheat sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

### Model Code References

They were too complicated to replicate/understand. But helped me grab the basics of subscription system/notification model.

1. [Real time notification system using Socket.io](https://codeforgeek.com/2015/09/real-time-notification-system-using-socket-io/)
2. [Nodejs and a simple push notification server](http://www.gianlucaguarini.com/blog/nodejs-and-a-simple-push-notification-server/)
3. [Creating a Push Notification Server with Node.js](http://shockoe.com/blog/creating-a-push-notification-server-with-node-js/)

Also, I figured the project was not about *push notifications* but instead about simple notification and subscription system. But I got the basic idea of what to implement from them.